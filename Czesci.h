#ifndef Czesci_HEADER
#define Czesci_HEADER

class Czesci  
{
private:
	char nazwa_czesci_zamiennej;
	char typ_czesci_zamiennej;
public:
	Czesci();
	Czesci (char, char);
	virtual ~Czesci();
	void wpisz_typ_czesci_zamiennej(char);
	void wpisz_nazwe_czesci_zamiennej(char);
	char wyswietl_nazwe_czesci_zamiennej();
	char wyswietl_typ_czesci_zamiennej();
	

};

#endif