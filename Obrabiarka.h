// Obrabiarka1.h: interface for the Obrabiarka class.
//
//////////////////////////////////////////////////////////////////////


#include <string>
#include "Naprawa.h"
#include "Czesci.h"

using namespace std;




class Obrabiarka  
{
private:
	int numer_obrabiarki; 

	Naprawa *tabl_naprawa[100];
	int liczba_napraw;

	Czesci *tabl_czesci[100];
	int liczba_czesci;
	
public:
	
	Obrabiarka();
	virtual ~Obrabiarka();
	void wpisz_numer_obrabiarki (int);
	int wyswietl_numer_obrabiarki();
	
		void dodaj_naprawe(string);
		void usun_naprawe(string);
		void wpisz_ilosc_napraw(int);
		void wpisz_rodzaj_naprawy(char);
		int wyswietl_ilosc_napraw(int);
		char wyswietl_rodzaj_naprawy();
		
			void dodaj_czesc_zamienna(string);
			void usun_czesc_zamienna(string);
			void wpisz_typ_czesci_zamiennej(string);
			void wpisz_nazwe_czesci_zamiennej(string);
			char wyswietl_nazwe_czesci_zamiennej();
			char wyswietl_typ_czesci_zamiennej();

};


