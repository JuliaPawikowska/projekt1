// Obrabiarka1.cpp: implementation of the Obrabiarka class.
//
//////////////////////////////////////////////////////////////////////


#include <iostream>
#include "Obrabiarka.h"
#include <string>
//#include "Naprawa.h"
//#include "Czesci.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Obrabiarka::Obrabiarka()
{
 liczba_napraw=0;
 liczba_czesci=0;
}

Obrabiarka::~Obrabiarka()
{

}

void Obrabiarka::wpisz_numer_obrabiarki(int a)
{
	numer_obrabiarki=a;
}

int Obrabiarka::wyswietl_numer_obrabiarki()
{
	return this->numer_obrabiarki;
}
	
//Naprawy
void Obrabiarka::dodaj_naprawe(string)
{
	tabl_naprawa[liczba_napraw]=new Naprawa;
	cout<<"Podaj numer nowej naprawy"<<endl;
	char a;
	cin>> a;
	tabl_naprawa[liczba_napraw]->wpisz_rodzaj_naprawy(a);
	liczba_napraw++;
	cout<<"Dodano nowa naprawe"<<endl;
}

void Obrabiarka::usun_naprawe(string)
{
	cout<<"usuwanie naprawy"<<endl;
	cout<<"Podaj numer naprawy, ktora chcesz usunac:"<<liczba_napraw-1<<":"<<endl;
	int i;
	cin>> i;
	delete tabl_naprawa [i];
	cout<<"Naprawa "<<i<<" zostala usunieta."<<endl;
	int j;
	for (j=i; j<liczba_napraw; j++)
	{
		tabl_naprawa[j]=tabl_naprawa[j+1];
	}
	liczba_napraw--;
	
}

void Obrabiarka::wpisz_ilosc_napraw(int)
{
 	cout<<"wpisywanie"<<endl;
}

void Obrabiarka::wpisz_rodzaj_naprawy(char)
{
	cout<<"wpisywanie"<<endl;
}

//Czesci zamienne
void Obrabiarka::dodaj_czesc_zamienna(string)
{
	cout<<"dodawanie czesci"<<endl;
	tabl_czesci[liczba_czesci]=new Czesci;
	cout<<"Podaj numer nowej czesci"<<endl;
	int a;
	cin>> a;
	tabl_czesci[liczba_czesci]->wpisz_typ_czesci_zamiennej(a);
	liczba_czesci++;
	cout<<"Dodano czesc zamienna"<<endl;
}

void Obrabiarka::usun_czesc_zamienna(string)
{
	cout<<"usuwanie czesci"<<endl;
	cout<<"Ktora czesc chcesz usunac:"<<liczba_czesci-1<<":"<<endl;
	int i;
	cin>> i;
	delete tabl_czesci [i];
	cout<<"Czesc "<<i<<" zostala usunieta."<<endl;
	int j;
	for (j=i; j<liczba_czesci; j++)
	{
		tabl_czesci[j]=tabl_czesci[j+1];
	}
	liczba_czesci--;
}

void Obrabiarka::wpisz_typ_czesci_zamiennej(string)
{
	cout<<"wpisywanie"<<endl;
}

void Obrabiarka::wpisz_nazwe_czesci_zamiennej(string)
{
	cout<<"wpisywanie"<<endl;
}

