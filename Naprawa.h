// Naprawa.h: interface for the Naprawa class.
//
//////////////////////////////////////////////////////////////////////



class Naprawa  
{
private:
	int ilosc_napraw;
	char rodzaj_naprawy;
public:
	Naprawa();
	Naprawa (int, char);
	virtual ~Naprawa();
	void wpisz_ilosc_napraw(int);
	void wpisz_rodzaj_naprawy(char);
	int wyswietl_ilosc_napraw(int);
	char wyswietl_rodzaj_naprawy();

};


